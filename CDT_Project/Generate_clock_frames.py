import os
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib;

matplotlib.use("TkAgg")
import seaborn as sns
import plotly.express as px

# For animation
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation, PillowWriter


def get_data():
    df_clock = pd.read_csv("clock_data\\clock-replay.csv")
    df_clock.rename(columns={'time (ms)': 'time_ms', 'x (pts)': 'x_pts', 'y (pts)': 'y_pts'}, inplace=True)
    return df_clock


""" Take user specific data and new thinking time column time_diff_ms """


def think_time_data(df_clock, block_id):
    # Get block/subject specific data
    data_block = (df_clock.loc[df_clock['block'] == block_id]).copy(deep=True)
    data_block.reset_index(inplace=True, drop=True)
    # Invert the y Axis
    data_block['y_pts'] = data_block['y_pts'] * -1
    # Get inbetween thinking time
    time_diff_ms = []
    t1 = 0
    for i, val in data_block.time_ms.iteritems():
        if i == 0:
            time_diff_ms.append(0)
        else:
            time_diff_ms.append(val - t1)
        t1 = val
    # convert list to dataframe column
    data_block["time_diff_ms"] = time_diff_ms
    data_directory(block_id)
    return data_block


def data_directory(id):
    directory = f"Generated_frames\\{id}\\"
    path = os.path.join(os.getcwd(), directory)
    try:
        if not os.path.exists(path):
            os.makedirs(path)
        else:
            print("Successfully created the directory %s " % path)
    except OSError:
        print("Creation of the directory %s failed" % path)


def CDT_movie(data_block, block_id, realtime_rate, custom_rate):
    """Animation scale"""
    if not realtime_rate:
        interval = custom_rate
    else:
        # speed_rate = data_block['time_diff_ms'][:1]+1
        interval = custom_rate

    """ Get top 18 values """
    time_diff = np.array(data_block['time_diff_ms'])

    """Update the plot for each point"""

    def update_plot(i, fig, scat):
        # _debug: Add +700 or large value to i to get co mpleted clock directly
        data = np.hstack((x[:i, np.newaxis], y[:i, np.newaxis]))
        scat.set_offsets(data)
        """ Get top 18 points with highest thinking time. Get smallest of 18 as threshold to capture the frame"""
        if int(time_diff[i]) >= int(data_block["time_diff_ms"].nlargest(16)[-1:]) or i == len(
                data_block['time_diff_ms']) - 1:
            fig.savefig(f'Generated_frames/{block_id}/Id{block_id}_Frame{i}_Delay{time_diff[i]}.png')
        if i == len(data_block['time_diff_ms']) - 1:
            plt.close(fig)
        return scat

    # % matplotlib notebook
    fig = plt.figure()
    x = data_block.x_pts
    y = data_block.y_pts

    """Can make plot circular instead of square with angles"""
    # https://matplotlib.org/3.1.3/gallery/pie_and_polar_charts/polar_scatter.html#sphx-glr-gallery-pie-and-polar-charts-polar-scatter-py
    ax = fig.add_subplot(111)
    #     fig.set_tight_layout(True)
    #     ax.grid(True, linestyle = '-', color = '0.75')

    """ Hide grid lines and axes ticks """
    ax.grid(False)
    ax.axis('off')
    ax.set_xticks([])
    ax.set_yticks([])

    """Boundaries of the plot"""
    plt.xlim(x.min() - 20, x.max() + 20)
    plt.ylim(y.min() - 20, y.max() + 20)

    #     scat = plt.scatter(x, y, color='green', linestyle='dashed', linewidth = 3,marker='o')
    scat = plt.scatter(x, y, c="black", marker='o')
    scat.set_alpha(0.8)

    cdt_movie = animation.FuncAnimation(fig, update_plot, fargs=(fig, scat),
                                        interval=interval, blit=False, repeat=False)
    # cdt_movie.save('line.gif', dpi=80, writer='imagemagick')
    plt.show()


def main():
    df_clock = get_data()
    for block_id in df_clock.block.unique():
        """ Get Subject specific drawing parameters: block and movie speed(lower is faster) """
        """Get induvidual data for each id"""
        data_block = think_time_data(df_clock, block_id)
        draw_rate = 2
        CDT_movie(data_block, block_id, True, draw_rate)
        """See think time plot"""
        # fig = px.line(data_block, x=None, y='time_diff_ms',
        #               hover_name=["Position: " + str(i) for i in range(len(data_block.time_ms))])
        # fig.update_layout(
        #     xaxis_title="Frames",
        #     yaxis_title="Inbetween Thinking time in millisecond")
        # fig.show()


if __name__ == main():
    main()
