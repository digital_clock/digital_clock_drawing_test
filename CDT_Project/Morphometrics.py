import math
import cv2
import matplotlib.pyplot as plt
import numpy as np
import skimage as sk
from skimage import measure


def load_image():
    img = cv2.imread('generated_frames/77/Id77_Frame112_Delay745.png')
    return img


def img_show(img1,msg1, img2, msg2):
    # plt.imshow(img)
    # plt.show()

    fig = plt.figure(figsize=(18, 8))
    plt.subplot(111), plt.imshow(img1)
    plt.title(msg1), plt.xticks([]), plt.yticks([])
    if len(msg2) > 2:
        plt.subplot(112), plt.imshow(img2)
        plt.title('bilateral_filtered_image'), plt.xticks([]), plt.yticks([])


def get_contours(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # convert to grayscale
    # blur = cv2.blur(gray, (3, 3))  # blur the image
    # Get Threshold
    ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)  # Try and estimate good value for threshold
    # Finding contours for the thresholded image
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # print(len(contours))
    return contours


def remove_gaps(img, cnt_len):
    """Kernel 5x5 size"""
    kernel = np.ones((3, 3), np.uint8)
    val = 0
    i = 0
    # for i in range(cnt_len):
    while True:
        erosion = cv2.erode(img, kernel, iterations=i)
        dilation = cv2.dilate(erosion, kernel, iterations=val)

        # gray = cv2.cvtColor(dilation, cv2.COLOR_BGR2GRAY)
        # blur = cv2.blur(gray, (3, 3))
        # ret, thresh = cv2.threshold(blur, 150, 255, cv2.THRESH_BINARY)
        # contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = get_contours(dilation)
        if i % 5 != 0:
            val += 1
        i += 1

        print(f"Contours at epoch {i}: {len(contours)}")

        if len(contours) <= 2 or i >= 60:
            # print(f"Erosion steps: {i} dilation steps:{val}")
            """ Can be use as global function to return contours and dilated+ eroded image 
                that can be use as input to other function.
                Adding +3 to erosion and dilation as buffer to avoid having some extra width to the image 
                to avoid loss of information like edges, pixels while resizing or scaling image.                
            """
            erosion = cv2.erode(img, kernel, iterations=i + 3)
            dilation = cv2.dilate(erosion, kernel, iterations=val + 3)
            print(f"Erosion steps: {i+3}, dilation steps:{val+3}")
            break
    # return dilation, contours, hierarchy
    return dilation


def scale_50(img):
    scale_percent = 50  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    print('Resized Dimensions : ', resized.shape)
    plt.imshow(resized, cmap='gray')
    return resized


def detected_circle(img):
    # load the image, clone it for output, and then convert it to grayscale
    image = img
    output = image.copy()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Blur using 3 * 3 kernel.
    gray_blurred = cv2.blur(gray, (3, 3))

    # Apply Hough transform on the blurred image.
    detected_circles = cv2.HoughCircles(gray_blurred, cv2.HOUGH_GRADIENT, 1, 500, param1=50,
                                        param2=25, minRadius=0, maxRadius=0)

    # Draw circles that are detected.
    if detected_circles is not None:
        print("Its a circle")
        # Convert the circle parameters a, b and r to integers.
        detected_circles = np.uint16(detected_circles)

        for pt in detected_circles[0, :]:
            a, b, r = pt[0], pt[1], pt[2]

            # Draw the circumference of the circle.
            cv2.circle(output, (a, b), r, (0, 255, 0), 2)

            # Draw a small circle (of radius 1) to show the center.
            cv2.circle(output, (a, b), 1, (0, 0, 255), 3)
            #         cv2.imshow("Detected Circle", img)
        # plt.imshow(output)
        # plt.show()
        return output, True


def check_circularity(img, th):
    res = img.copy()
    print("Threshold value : {}".format(th))
    """Convert to gray"""
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    """ Threshold the input image to obtain a binary image. Threshold the gray"""
    th, threshed = cv2.threshold(gray, th, 255, cv2.THRESH_BINARY)
    """Find contours on the binary threshed image """
    contours = cv2.findContours(threshed, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2]
    """calcualte """
    for cnt in contours:
        arclen = cv2.arcLength(cnt, True)
        area = cv2.contourArea(cnt)
        cv2.drawContours(res, [cnt], -1, (0, 255, 0), 3, cv2.LINE_AA)
        print("Arc Length: {:.3f}\nArea: {:.3f}".format(arclen, area))


    plt.imshow(res, cmap='gray')
    area = (cv2.contourArea(contours[0]) + cv2.contourArea(contours[1])) / 2
    perimeter = (cv2.arcLength(contours[0], True) + cv2.arcLength(contours[1], True)) / 2
    # area = cv2.contourArea(contours[0])
    # perimeter = cv2.arcLength(contours[0],True)
    print("area of circle: ", area)
    print("perimeter of circle: ", round(perimeter, 2))
    print("   ")
    circularity = (4 * 3.14 * area) / perimeter ** 2
    print("Circularity of drawn circle:", round(circularity, 4))

    """ Circularity: other approach for validation"""
    print("\n CIRCULARITY: Other approach: Minimum enclosing Circle")
    (x, y), radius = cv2.minEnclosingCircle(contours[0])
    center = (int(x), int(y))
    print("center: ", center)
    radius = int(radius)
    print("radius: ", radius)
    fitted = cv2.circle(img, center, radius, (0, 255, 0), 2)
    plt.imshow(fitted, cmap='gray')

    area_ft = 3.14 * (radius) ** 2
    print("Area of fitted circle: ", area_ft)
    print("Area of drawn circle: ", area)

    area_diff = area_ft - area
    print("Area difference: ", round(area_diff, 4))

    area_ratio = area / area_ft
    print("Area ratio: ", round(area_ratio, 4))


def get_elongation(img, cnts):
    props = sk.measure.regionprops(img)
    maj_ax_le = round(props[0].major_axis_length, 3)
    print("Major axis: ", maj_ax_le)
    min_ax_le = round(props[0].minor_axis_length, 3)
    print("minor axis: ", min_ax_le)
    """ Above method seems inaccurate; need more evaluation"""

    cnt = cnts[1]  # consider outer contour
    leftmost = tuple(cnt[cnt[:, :, 0].argmin()][0])
    rightmost = tuple(cnt[cnt[:, :, 0].argmax()][0])
    topmost = tuple(cnt[cnt[:, :, 1].argmin()][0])
    bottommost = tuple(cnt[cnt[:, :, 1].argmax()][0])
    print(leftmost, rightmost, topmost, bottommost)

    # calculate distance between two points
    hor_ax_size = math.sqrt(((leftmost[0] - rightmost[0]) ** 2) + ((leftmost[1] - rightmost[1]) ** 2))
    print("Horizontal axis: ", hor_ax_size)
    ver_ax_size = math.sqrt(((topmost[0] - bottommost[0]) ** 2) + ((topmost[1] - bottommost[1]) ** 2))
    print("Vertical axis: ", ver_ax_size)

    if hor_ax_size > ver_ax_size:
        print("drawn  circle is horizontally streached: ")
        rate = hor_ax_size / ver_ax_size
        print("Rate: ", rate)
    else:
        print("drawn  circle is vertically streached: ")
        rate = ver_ax_size / hor_ax_size
        print("Rate: ", rate)


def main():
    """static code: for test"""
    img = load_image()

    """ Get total contours of row image"""
    contours = get_contours(img)
    print(f"Total contours of row image: {len(contours)}")

    """Remove gaps between circle and get total dilation and erosion steps needed to do so"""
    img_er_dil = remove_gaps(img, len(contours))

    """" Check if it a circle or not
         If its not a circle perform:   1. go to next image or final image to check if it has a circle. check for next 3 images
                                        2. check if it is a arc or incomplete circle
                                        3. calculate distance between start and end point of arc
         If"""
    img_show(img_er_dil)
    im_cr, im_cr_detected = detected_circle(img)
    if im_cr_detected:



if __name__ == main():
    main()
